package exercise;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] arr) {
        int counter = 0;
        int secondCounter = 0;

        if (arr.length == 0) {
            return arr;
        }

        int averageResult = getLessAverage(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= averageResult) {
                counter++;
            }
        }
        int[] result = new int[counter];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= averageResult) {
                result[secondCounter] = arr[i];
                secondCounter++;
            }
        }
        return result;
    }

    public static int getLessAverage(int[] averageArr) {
        int average = 0;
        for (int i = 0; i < averageArr.length; i++) {
            average += averageArr[i];
        }
        return average / averageArr.length;
    }

    /*
    САМОСТОЯТЕЛЬНАЯ РАБОТА
    */
    public static int getSumBeforeMinAndMax(int[] arr) {
        int currentMin = arr[0];
        int currentMinIndex = 0;
        int currentMax = arr[0];
        int currentMaxIndex = 0;
        int result = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > currentMax) {
                currentMax = arr[i];
                currentMaxIndex = i;
            }
            if (arr[i] < currentMin) {
                currentMin = arr[i];
                currentMinIndex = i;
            }
        }

        for (int k = currentMaxIndex + 1; k < currentMinIndex; k++) {
            result += arr[k];
        }
        return result;
    // END
    }
}
