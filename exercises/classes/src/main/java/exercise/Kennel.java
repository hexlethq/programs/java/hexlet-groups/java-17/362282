package exercise;

import java.util.Arrays;

// BEGIN
public class Kennel {
    private static String[][] puppies = new String[][]{};
    private static int puppiesCount = 0;

    public static void addPuppy(String[] newPuppy) {
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[puppies.length - 1] = newPuppy;
        puppiesCount++;
    }

    public static void addSomePuppies(String[][] newFewPuppies) {
        puppies = Arrays.copyOf(puppies, puppies.length + newFewPuppies.length);
        for (String[] newPuppy : newFewPuppies) {
            puppies[puppiesCount] = newPuppy;
            puppiesCount++;
        }
    }

    public static int getPuppyCount() {
        return puppiesCount;
    }

    public static boolean isContainPuppy(String puppyName) {
        for (int i = 0; i < puppies.length - 1; i++) {
            if (puppies[i][0].equals(puppyName)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] namesOfPuppies = new String[puppies.length];
        int counter = 0;

        for (String[] puppy: puppies) {
            if (puppy[1].equals(breed)) {
                namesOfPuppies[counter] = puppy[0];
                counter++;
            }
        }
        return counter > 0 ? Arrays.copyOf(namesOfPuppies, counter) : new String[0];
    }

    public static void resetKennel() {
        puppies = new String[][]{};
        puppiesCount = 0;
    }
}
// END
