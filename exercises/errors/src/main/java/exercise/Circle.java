package exercise;

// BEGIN
public class Circle {
    private static Point point;
    private static int radius;

    public Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    public int getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (radius >= 0) {
            return Math.PI * Math.pow(radius, 2);
        } else {
            throw new NegativeRadiusException("Error! NegativeRadiusException");
        }
    }
}
// END
