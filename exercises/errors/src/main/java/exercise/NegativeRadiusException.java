package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {
    public NegativeRadiusException(String errorDescription) {
        super(errorDescription);
    }
}
// END
