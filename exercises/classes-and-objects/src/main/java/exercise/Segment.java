package exercise;

// BEGIN
public class Segment {
    Point firstPoint;
    Point secondPoint;

    public Segment(Point firstPoint, Point secondPoint) {
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }

    public Point getBeginPoint() {
        return firstPoint;
    }

    public Point getEndPoint() {
        return secondPoint;
    }

    public Point getMidPoint() {
        int firstCoordOfPoint = (firstPoint.getX() + secondPoint.getX()) / 2;
        int secondCoordOfPoint = (firstPoint.getY() + secondPoint.getY()) / 2;
        return new Point(firstCoordOfPoint, secondCoordOfPoint);
    }
}
// END
