package exercise;

// BEGIN
public class Point {
    private final int xCoordinate;
    private final int yCoordinate;

    public Point(int x, int y) {
        this.xCoordinate = x;
        this.yCoordinate = y;
    }

    public int getX() {
        return xCoordinate;
    }

    public int getY() {
        return yCoordinate;
    }
}
// END
