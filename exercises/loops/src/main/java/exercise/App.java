package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String result = "";
        String temp = phrase.trim().replaceAll(" +", " ");

        for (int i = 0; i < temp.length(); i++) {
            char currentChar = temp.charAt(i);
            if (currentChar == ' ') {
                result += temp.charAt(i + 1);
            }
        }
        return (temp.charAt(0) + result).toUpperCase();
    }
    // END
}
