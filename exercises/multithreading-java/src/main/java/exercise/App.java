package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;




class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] arr) {
        Thread minThread = new MinThread(arr);
        Thread maxThread = new MaxThread(arr);
        minThread.start();
        LOGGER.log(Level.INFO, "MinThread started!");
        maxThread.start();
        LOGGER.log(Level.INFO, "MaxThread started!");
        try {
            minThread.join();
            LOGGER.log(Level.INFO, "MinThread finished");
            maxThread.join();
            LOGGER.log(Level.INFO, "MaxThread finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return Map.of("min", MinThread.getMin(),
                      "max", MaxThread.getMax());
    }
    // END
}

