package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] arr = {x, y};
        return arr;
    }

    public static int getX(int[] xArray) {
        return xArray[0];
    }

    public static int getY(int[] yArray) {
        return yArray[1];
    }

    public static String pointToString(int[] arrToString) {
        String result = "(" + arrToString[0] + ", " + arrToString[1] + ")";
        return result;
    }

    public static int getQuadrant(int[] arrQuadrant) {
        int result = 0;
        int x = arrQuadrant[0];
        int y = arrQuadrant[1];

        if (x > 0 && y > 0) {
            result = 1;
        } else if (x > 0 && y < 0) {
            result = 4;
        } else if (x < 0 && y > 0) {
            result = 2;
        } else if (x < 0 && y < 0) {
            result = 3;
        }

        return result;
    }
    // END
}
