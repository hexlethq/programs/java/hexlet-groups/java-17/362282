package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import exercise.Article;
import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;


public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                try {
                    showArticles(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    showArticle(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private int getPage(HttpServletRequest request) {
        String currentPage = request.getParameter("page");
        return Objects.equals(currentPage, null) ? 1 : Integer.parseInt(currentPage);
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
            throws IOException, ServletException, SQLException {
        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT * FROM articles ORDER BY id LIMIT 10 OFFSET ?");
        int currentPage = getPage(request);
        preparedStatement.setInt(1, (currentPage * 10) - 10);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Article> articleList = new ArrayList<>();
        while (resultSet.next()) {
            Article article = new Article();
            article.setId(resultSet.getLong("id"));
            article.setBody(resultSet.getString("body"));
            article.setTitle(resultSet.getString("title"));
            articleList.add(article);
        }

        request.setAttribute("articles", articleList);
        request.setAttribute("currentPage", currentPage);
        preparedStatement.close();
        resultSet.close();
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException, ServletException, SQLException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        int articleId = Integer.parseInt(getId(request));
        if (articleId > 100) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT title, body FROM articles WHERE ID = ?");
        preparedStatement.setInt(1, articleId);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.first();
        request.setAttribute("title", resultSet.getString(1)); // тут все работает, если я итерирую
        request.setAttribute("body", resultSet.getString(2));
        preparedStatement.close();
        resultSet.close();
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
