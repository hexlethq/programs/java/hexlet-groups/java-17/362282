package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AppTest {
    List<Integer> numbers;

    @BeforeEach
    void initListOfNumbers() {
        this.numbers = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6, 7, 8));
    }

    @Test
    void testTake() {
        // BEGIN
        List<Integer> expected = new ArrayList<>(List.of(1, 2, 3, 4, 5));
        Assertions.assertEquals(App.take(numbers, 5), expected);
        // END
    }

    @Test
    void testTake_withZeroCountOfDigits() {
        org.assertj.core.api.Assertions.assertThat(App.take(numbers, 0))
                .isEqualTo(new ArrayList<>());
    }

    @Test
    void testTake_withEmptyList() {
        List<Integer> list = new ArrayList<>();
        org.assertj.core.api.Assertions.assertThat(App.take(list, 5))
                .isEqualTo(new ArrayList<>());
    }

    @Test
    void testTake_withNegativeDigitInArguments() {
        Assertions.assertEquals(App.take(numbers, -2), new ArrayList<>());
    }

    @Test
    void testTake_withArgumentOutOfRangeOfList(){
        Assertions.assertEquals(App.take(numbers, 20), numbers);
    }
}
