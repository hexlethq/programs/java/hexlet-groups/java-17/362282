package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        String query = request.getQueryString();
        String param = request.getParameter("search");
        String companies = "Companies not found";
        List<String> companyList = getCompanies();

        if (query == null || param.equals("")) {
            companies = String.join("\n", companyList);
        } else {
            List<String> searchList = new ArrayList<>();
            for (String company: companyList) {
                if (param != null && company.contains(param)) {
                    searchList.add(company);
                }
            }
            if (searchList.size() > 0) {
                companies = String.join("\n", searchList);
            }
        }

        PrintWriter out = response.getWriter();
        out.println(companies);
        // END
    }
}
