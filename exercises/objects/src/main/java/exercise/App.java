package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");

        if (arr.length == 0) {
            return "";
        }

        for (int i = 0; i < arr.length; i++) {
            sb.append("  <li>" + arr[i] + "</li>\n");
        }
        sb.append("</ul>");
        return sb.toString();
    }

    public static String getUsersByYear(String[][] arr, int date) {
        boolean isSbEmpty = true;
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");

        if (arr.length == 0 || arr[0].length == 0) {
            return "";
        }

        for (int i = 0; i < arr.length; i++) {
            String strTemp = arr[i][1].substring(0, 4);
            int intTemp = Integer.parseInt(strTemp);
            if (intTemp == date) {
                sb.append("  <li>" + arr[i][0] + "</li>\n");
                isSbEmpty = false;
            }
        }

        if (isSbEmpty) {
            return "";
        }

        sb.append("</ul>");
        return sb.toString();
        // END

        // Это дополнительная задача, которая выполняется по желанию.
        //public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN

        // END
        //}
    }
}
