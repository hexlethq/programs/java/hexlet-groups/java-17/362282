package exercise;

class Converter {
    // BEGIN
    public static int convert(int digit, String dataType) {
        if (dataType.equals("b")) {
            return digit * 1024;
        } else if (dataType.equals("Kb")) {
            return digit / 1024;
        } else {
            return 0;
        }
    }
    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
