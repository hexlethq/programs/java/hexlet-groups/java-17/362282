package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int firstSide, int secondSide, int degree) {
        double degreeToRadians = (degree * Math.PI) / 180;
        double result = ((firstSide * secondSide) / 2) * Math.sin(degreeToRadians);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
