package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        Path path = Paths.get("src/main/resources/users.json").toAbsolutePath().normalize();
        String temp = Files.readString(path);
        ObjectMapper om = new ObjectMapper();

        List<Map<String, String>> result = om.readValue(temp, new TypeReference<List<Map<String, String>>>() { });
        return result;
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();

        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>"
                + "<html lang = \"ru\""
                + "<head>"
                + "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\""
                + " rel=\"stylesheet\" integrity=\"sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09"
                + "zVXn8CA5QIVfZOJ3BCsw2P0p/We\" "
                + "crossorigin=\"anonymous\">"
                + "<title>Example application | Users</title>"
                + "</head>"
                + "<body>"
                + "<style>\n"
                + "  table {\n"
                + "    border-collapse: collapse;\n"
                + "  }\n"
                + "  th, td {\n"
                + "    border: 1px solid black;\n"
                + "    padding: 10px;\n"
                + "    text-align: left;\n"
                + "  }\n"
                + "</style>"
                + "<table>"
                + "<tr>"
                + "<th>ID</th>"
                + "<th>fullName</th>"
                + "</tr>");

        for (Map map : users) {
            sb.append("<tr>" + "<td>" + map.get("id") + "</td>" + "<td>" + "<a href=\"/users/" + map.get("id") + "\">"
                     + map.get("firstName") + " " + map.get("lastName") + "</a>"
                     + "</td></tr>");
        }
        sb.append("</table></body></html>");
        PrintWriter pw = response.getWriter();
        pw.println(sb);
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        StringBuilder sb = new StringBuilder();
        List<Map<String, String>> list = getUsers();
        Map<String, String> user = null;

        for (Map map : list) {
            if (map.get("id").equals(id)) {
                user = new HashMap<>(map);

                sb.append("<!DOCTYPE html>"
                        + "<html lang = \"ru\""
                        + "<head>"
                        + "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\""
                        + " rel=\"stylesheet\" integrity=\"sha384"
                        + "-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We\""
                        + " crossorigin=\"anonymous\">"
                        + "<title>Example application | Users</title>"
                        + "</head>"
                        + "<body>"
                        + "<style>\n"
                        + "  table {\n"
                        + "    border-collapse: collapse;\n"
                        + "  }\n"
                        + "  th, td {\n"
                        + "    border: 1px solid black;\n"
                        + "    padding: 10px;\n"
                        + "    text-align: left;\n"
                        + "  }\n"
                        + "</style>"
                        + "<table>"
                        + "<tr>"
                        + "<th>ID</th>"
                        + "<th>firstName</th>"
                        + "<th border=\"1\">lastName</th>"
                        + "<th>email</th>"
                        + "</tr>"
                        + "<tr>"
                        + "<td>" + user.get("id") + "</td>"
                        + "<td>" + user.get("firstName") + "</td>"
                        + "<td>" + user.get("lastName") + "</td>"
                        + "<td>" + user.get("email") + "</td></tr>"
                        + "</table></body></html>"
                );
                PrintWriter pw = response.getWriter();
                pw.println(sb);
                return;
            }
        }
        response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
        // END
    }
}
