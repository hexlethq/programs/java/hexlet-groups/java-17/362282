package exercise;

import java.util.*;

// BEGIN
public class App {
    public static List<Map> findWhere(List<Map> library,
                                      Map<String, String> keys) {
        List<Map> result = new ArrayList<>(library);
        for (Map book : library) {
            for (String key : keys.values()) {
                if (!book.containsValue(key)) {
                    result.remove(book);
                }
            }
        }
        return result;
    }
}
//END
