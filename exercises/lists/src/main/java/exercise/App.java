package exercise;

import java.util.List;
import java.util.ArrayList;

// BEGIN
public class App {
    public static boolean scrabble(String chars, String word) {
        if (chars.isEmpty() || chars.length() < word.length()) {
            return false;
        }
        List<Character> charsToMakeWord = new ArrayList<>();
        for (char c : chars.toLowerCase().toCharArray()) {
            charsToMakeWord.add(c);
        }

        for (int i = 0; i < word.length(); i++) {
            Character charToRemove = Character.toLowerCase(word.charAt(i));
            boolean isCharRemoved = charsToMakeWord.remove(charToRemove);
            if (!isCharRemoved) {
                return false;
            }
        }
        return true;
    }
}
//END
