package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String data) {
        Stream<String> stream = Arrays.stream(data.split("\n"));
        String result = stream
                .filter(x -> x.startsWith("environment"))
                .filter(x -> x.contains("X_FORWARDED_"))
                .map(x -> editVariables(x))
                .collect(Collectors.joining(","));
        return result;
    }

    public static String editVariables(String editString) {
        Stream<String> stream = Arrays.stream(editString.split(","));
        String result = stream
                .map(x -> x.replaceAll("environment=",""))
                .map(x -> x.replaceAll("\"", ""))
                .filter(x -> x.startsWith("X_FORWARDED_"))
                .map(x -> x.replaceAll("X_FORWARDED_",""))
                .map(x -> x.replaceAll(",",""))
                .collect(Collectors.joining(","));
        return result;
    }
}
//END
