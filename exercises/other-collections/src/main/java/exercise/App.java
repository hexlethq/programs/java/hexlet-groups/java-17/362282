package exercise;

import java.util.*;

// BEGIN
public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> firstMap, Map<String, Object> secondMap) {
        Map<String, String> temp = new TreeMap<>();

        if (firstMap.isEmpty()) {
            for (String key : secondMap.keySet()) {
                temp.put(key, "added");
            }
        }

        if (secondMap.isEmpty()) {
            for (String key : firstMap.keySet()) {
                temp.put(key, "deleted");
            }
        }

        for (String firstKey : firstMap.keySet()) {
            for (String secondKey : secondMap.keySet()) {
                if (secondMap.containsKey(secondKey) && !firstMap.containsKey(secondKey)) {
                    temp.put(secondKey, "added");
                }
                if (firstMap.containsKey(firstKey) && !secondMap.containsKey(firstKey)) {
                    temp.put(firstKey, "deleted");
                }
                if (firstMap.containsKey(firstKey) && secondMap.containsKey(firstKey) && !firstMap.get(firstKey).equals(secondMap.get(firstKey))) {
                    temp.put(firstKey, "changed");
                }
                if (firstMap.containsKey(firstKey) && secondMap.containsKey(secondKey) && firstMap.get(firstKey).equals(secondMap.get(firstKey))) {
                    temp.put(firstKey, "unchanged");
                }
            }
        }
        return new LinkedHashMap<>(temp);
    }
}
//END
