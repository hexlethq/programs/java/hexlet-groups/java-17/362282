package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static HashMap<String, Integer> getWordCount(String sentence) {
        if (sentence.isEmpty()) {
            return new HashMap();
        }
        String[] sentenceSplit = sentence.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        for (String s : sentenceSplit) {
            if (!result.containsKey(s)) {
                result.put(s, 1);
            } else {
                result.put(s, result.get(s) + 1);
            }
        }
        return result;
    }

    public static String toString(HashMap<String, Integer> sentence) {
        if (sentence.isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Map.Entry<String, Integer> map : sentence.entrySet()) {
            sb.append("\n  ");
            sb.append(map.getKey() + ": ");
            sb.append(map.getValue());
        }
        sb.append("\n}");
        return String.valueOf(sb);
    }
}
//END
