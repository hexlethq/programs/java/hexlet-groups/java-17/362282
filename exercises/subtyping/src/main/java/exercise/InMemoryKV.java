package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
    private Map<String, String> base;

    public InMemoryKV(Map<String, String> base) {
        this.base = new HashMap<>(base);
    }

    @Override
    public void set(String key, String value) {
        base.put(key, value);
    }

    @Override
    public void unset(String key) {
        base.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        return base.containsKey(key) ? base.get(key) : defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(base);
    }
}
// END
