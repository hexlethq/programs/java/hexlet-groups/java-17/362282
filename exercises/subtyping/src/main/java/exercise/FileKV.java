package exercise;

import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {
    private String filePath;
    private Map<String, String> base;

    public FileKV(String filePath, Map<String, String> base) {
        this.filePath = filePath;
        this.base = base;
        Utils.writeFile(filePath, base.toString());
        Utils.serialize(base);
    }

    @Override
    public void set(String key, String value) {
        base.put(key, value);
        Utils.writeFile(filePath, base.toString());
    }

    @Override
    public void unset(String key) {
        base.remove(key);
        Utils.writeFile(filePath, base.toString());
    }

    @Override
    public String get(String key, String defaultValue) {
        return base.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return base;
    }
}
// END
