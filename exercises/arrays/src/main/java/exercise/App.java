package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }
        return arr;
    }

    public static int getIndexOfMaxNegative(int[] arr) {
        int min = -1;
        int currentMaxNegative = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > currentMaxNegative) {
                min = i;
                currentMaxNegative = arr[i];
            }
        }
        return min;
        // END
    }
}
