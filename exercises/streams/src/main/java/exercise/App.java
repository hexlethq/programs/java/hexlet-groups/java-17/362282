package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> list) {
        return list.stream()
                .filter(mail -> mail.endsWith("@gmail.com")
                        || mail.endsWith("@yandex.ru")
                        || mail.endsWith("@hotmail.com"))
                .collect(Collectors.counting());
    }
}
// END
