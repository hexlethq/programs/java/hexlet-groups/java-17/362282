package exercise;

import java.util.ArrayList;
import java.util.List;

class SafetyList {
    // BEGIN
    private List<Integer> collection = new ArrayList<>();

    public synchronized void add(Integer data) {
        collection.add(data);
    }

    public Integer get(int index) {
        return collection.get(index);
    }

    public int getSize() {
        return collection.size();
    }
    // END
}
