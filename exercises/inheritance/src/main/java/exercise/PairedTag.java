package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {
    String body;
    List<Tag> kids;

    PairedTag(String name, Map<String, String> attributes, String body, List<Tag> kids) {
        super(name, attributes);
        this.body = body;
        this.kids = kids;
    }

    /*
    Здесь можно было бы написать через кучу доп.строк для наглядности, но мне кажется, что в этом нет смысла.
    Так как таким образом код выглядит короче, и структура построения читается намного лучше, чем делать через кучу
    конкатенаций и тд.
     */

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<")
                .append(getName())
                .append(stringifyAttributes())
                .append(">")
                .append(body);

        for (Tag tag : kids) {
            sb.append(tag);
        }

        sb.append("</")
                .append(getName())
                .append(">");
        return sb.toString();
    }
}
// END
