package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        boolean isExist = false;
        String result = "";


        if (a + b > c && b + c > a && c + a > b) {
            isExist = true;
        } else if (!isExist) {
            result = "Треугольник не существует";
        }

        if ((isExist) && (a == b && b == c && a == c)) {
            result = "Равносторонний";
        } else if ((isExist) && (a == b || b == c || a == c)) {
            result = "Равнобедренный";
        } else if ((isExist) && (a != b && b != c && a != c)) {
            result = "Разносторонний";
        }
        return result;
    }

    /*
    САМОСТОЯТЕЛЬНАЯ РАБОТА
     */
    public static int getFinalGrade(int exam, int project) {
        if (exam > 90 || project > 10) {
            return 100;
        } else if (exam > 75 && project >= 5) {
            return 90;
        } else if (exam > 50 && project >= 2) {
            return 75;
        } else {
            return 0;
        }
    }
    // END
}
