package exercise.controller;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;
import exercise.CityNotFoundException;
import exercise.dto.WeatherDto;
import exercise.model.City;
import exercise.model.QCity;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import liquibase.pro.packaged.S;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    private ObjectMapper om = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public WeatherDto getCityById(@PathVariable(name = "id") long id) throws JsonProcessingException {
        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City with such ID not found"));

        return om.readValue(weatherService.getWeather(city.getName()), WeatherDto.class);
    }

    @GetMapping(path = "/search")
    public List<WeatherDto> getCities(@RequestParam(name = "name", required = false) String name) throws JsonProcessingException {
        List<WeatherDto> weatherDtos = new ArrayList<>();
        if (name != null) {
            List<City> cities =  cityRepository.findAllByNameStartsWithIgnoreCase(name);

            for (City c : cities) {
                weatherDtos.add(om.readValue(weatherService.getWeather(c.getName()), WeatherDto.class));
            }
            return weatherDtos;
        }

        List<City> cities = cityRepository.findAllByOrderByNameAsc();

        for (City c : cities) {
            weatherDtos.add(om.readValue(weatherService.getWeather(c.getName()), WeatherDto.class));
        }

        return weatherDtos;
    }
    // END
}

