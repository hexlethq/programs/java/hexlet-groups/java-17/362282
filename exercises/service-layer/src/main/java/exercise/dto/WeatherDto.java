package exercise.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import liquibase.pro.packaged.S;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WeatherDto {
    private String temperature;
    private String name;
    private String cloudy;
    private String wind;
    private String humidity;

    public WeatherDto(String temperature, String name) {
        this.temperature = temperature;
        this.name = name;
    }
}
