package exercise.controllers;

import io.javalin.core.validation.ValidationError;
import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;
import java.util.Map;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> list = new QUser().findList();
        ctx.json(DB.json().toJson(list));
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
                .id.equalTo(Integer.parseInt(id))
                .findOne();
        ctx.json(DB.json().toJson(user));
        // END
    };

    public void create(Context ctx) {
        // BEGIN
        Map<String, List<ValidationError<User>>> errorList = ctx.bodyValidator(User.class)
                .check(u -> EmailValidator.getInstance().isValid(u.getEmail()), "Email is not valid")
                .check(u -> StringUtils.isNumeric(u.getPassword()), "Password must contains only digits")
                .check(u -> !u.getFirstName().isBlank(), "First name can not be empty")
                .check(u -> !u.getLastName().isBlank(), "Last name can not be empty")
                .errors();
        if (!errorList.isEmpty()) {
            ctx.status(422);
            ctx.json(errorList);
            return;
        }
        User user = DB.json().toBean(User.class, ctx.body());
        user.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        User user = DB.json().toBean(User.class, ctx.body());
        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        User user = new QUser()
                .id.equalTo(Integer.parseInt(id)).findOne();
        user.delete();
        // END
    };
}
