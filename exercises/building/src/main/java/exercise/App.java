// BEGIN
package exercise;

import com.google.gson.Gson;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public static String toJson(String[] arr) {
        Gson json = new Gson();
        String result = json.toJson(arr);
        return result;
    }
}
// END
