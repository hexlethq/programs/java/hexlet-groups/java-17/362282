package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String star = "*";
        System.out.println(star.repeat(starsCount) + cardNumber.substring(12, 16));
        // END
    }
}
