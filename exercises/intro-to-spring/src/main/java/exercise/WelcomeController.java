package exercise;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

// BEGIN
@RestController
public class WelcomeController {
    @GetMapping("/")
    public String welcome() {
        return "Welcome to Spring";
    }
    @GetMapping("/hello")
    public String hello(@RequestParam(required = false) String name) {
        return name == null ? "Hello, World" : "Hello, " + name;
    }
}
// END
