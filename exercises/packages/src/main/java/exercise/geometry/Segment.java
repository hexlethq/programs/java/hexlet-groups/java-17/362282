// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] firstPoint, double[] secondPoint) {
        double[][] segment = new double[2][2];
        segment[0][0] = firstPoint[0];
        segment[0][1] = firstPoint[1];
        segment[1][0] = secondPoint[0];
        segment[1][1] = secondPoint[1];
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint = {segment[0][0], segment[0][1]};
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endPoint = {segment[1][0], segment[1][1]};
        return endPoint;
    }
}
// END
