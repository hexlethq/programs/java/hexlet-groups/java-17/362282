// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

import java.util.Arrays;

public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] midpoint = new double[2];
        midpoint[0] = (segment[0][0] + segment[1][0]) / 2;
        midpoint[1] = (segment[0][1] + segment[1][1]) / 2;
        return midpoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[][] resultReverse = new double[2][2];
        resultReverse[0][0] = segment[1][0];
        resultReverse[0][1] = segment[1][1];
        resultReverse[1][0] = segment[0][0];
        resultReverse[1][1] = segment[0][1];
        return resultReverse;
    }
}
// END
