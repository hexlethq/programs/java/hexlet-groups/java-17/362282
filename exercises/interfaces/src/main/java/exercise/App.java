package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> list, int countOfExamples) {
        List<String> result = list.stream()
                    .sorted(Home::compareTo)
                    .limit(countOfExamples)
                    .map(Home::toString)
                    .collect(Collectors.toList());
        return result;
    }
}
// END
