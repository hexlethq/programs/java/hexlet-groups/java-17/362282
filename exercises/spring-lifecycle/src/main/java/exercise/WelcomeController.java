package exercise;

import exercise.daytimes.Daytime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {
    @Autowired
    private Daytime dayTime;

    @Autowired
    private Meal meal;

    @GetMapping("/daytime")
    public String getMealByDaytime() {
        String dayTimeName = dayTime.getName();

        return "It is " + dayTimeName + " now. Enjoy your " + meal.getMealForDaytime(dayTimeName);
    }
}
// END
